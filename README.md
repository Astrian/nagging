# 碎碎念
「碎碎念」是一款私人微型博客工具。有了它，你可以自由、随性地向世界分享你的所思所想，并且不受任何无关信息打扰。

「碎碎念」不是：

- **社交网络。** 因此它不会有多用户、通知系统和关系链功能。
- **CMS（内容管理系统）。** 它将专注于短消息发布。虽然理论上可以做到，但长内容发布并不是它的强项。
- **即时通讯工具。** 它并没有任何私密性的要求与可能。

「碎碎念」的原型是 daibor 开发的 [b 言 b 语](https://bb.daibor.com/)，在此表示感谢。

## 安装与配置
### 0. 配置环境
需要安装：

- Node.js 10.0+（版本过高可能导致部分模块无法正常工作，待测试）
- [Vue Cli](https://cli.vuejs.org)

推荐安装：

- Yarn（也可以使用 npm）
- pm2（可以不使用，也可以使用其他的 Node 运维工具）

不需要安装：

- 数据库程序。「碎碎念」使用 SQLite，并在代码中内嵌了一个空的 SQLite 数据库。

### 1. 下载代码并安装依赖
```
cd /var/www
git clone git@gitlab.com:Astrian/nagging.git
cd nagging
```

### 2. 配置碎碎念程序
复制空数据库及配置文件：

```
cp src/config.empty.js src/config.js
cp database.empty.db database.db
```

修改服务器端配置文件：

```
vi src/config.js
```

配置文件如下：

```js
const config = {
  website: {
    domain: "https://<your_domain>"
  }
}

export default config 
```

字段解释：

- `website`：网站服务相关设置
  - `domain`：你的碎碎念域名

设置完成后，使用以下指令开启服务器：

- 开启 GraphQL 服务器：`yarn apollo:start`
- 编译 SSR（Server-Side Rendering）：`yarn ssr:build`
- 开启 SSR：`yarn ssr:start`

使用 `pm2` 来运行这些服务：`pm2 "yarn <command>"`。

### 3. 配置服务器
这里以 Nginx 配置文件为例。请根据现实情况进行调整。

```
# 前端
server {
  listen 80;
  server_name <your_domain>;
  # SSR 服务
  location / {
    proxy_pass http://127.0.0.1:8000;
  }
  # GraphQL 服务
  location = /graphql {
    proxy_pass http://127.0.0.1:4000;
  }
}
```

**请注意**：以上 `proxy_pass` 中的端口号可能会有所不同（特别是在你的服务器有其他 GraphQL 或 Vue SSR 服务的前提下）。如果此设置无法正常访问碎碎念服务，请根据上一步的开启服务器的指令输出结果，调整以上端口号。

保存后，重新载入一次 Nginx。

### 4. SSL 签证
这一步并非必要，但是强烈建议的。

请参考：[使用 Certbot 进行签证](https://certbot.eff.org/)。

### 5. 初始化帐户
访问 `https://<your_domain>/graphql`，将以下代码粘贴进去：

```gql
mutation {
  signup(
    username: "<your_username>"
    password: "<your_password>"
    email: "<your_email_address>"
    bio: "<your_brief_profile>"
  )
}
```

记得修改以下值：
- `<your_username>` 改成你的碎碎念用户名
- `<your_password>` 改成你碎碎念的密码
- `<your_email_address>` 改成你的邮箱地址（用于获取 Gravatar 头像，*不作为找回密码的方式*）
- `<your_brief_profile>` 改成你的个人简介

点击页面中间的「播放」按钮，右侧没有提示错误信息，便代表你的帐户初始化完成。

### 6. 登录
在浏览器中访问：

`https://<your_frontend_domain>/login`

登录成功后将直接进入你的碎碎念主页。之后，别人只需要访问你的前端域名，就可以查看你的碎碎念了。

### 7. 偏好设置
`https://<your_frontend_domain>/preference/security`

## 协议
MIT