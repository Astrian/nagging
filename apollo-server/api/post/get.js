const debug = require('debug')('fgm:api/post/get')
const database = require('../../plugins/database')
const md5 = require('md5')

module.exports = async (page) => {
  if (!page) page = 1
  let dbres = await database('r', 'SELECT * FROM posts ORDER BY pubdate DESC LIMIT 10 OFFSET ?', [(page - 1) * 10])
  for (let i in dbres) {
    let user = (await database('r', 'SELECT id, bio, posts, username, fullname, avatar, email FROM users WHERE id = ?', [dbres[i].author]))[0]
    if (user.avatar === '!gravatar') user.avatar = `https://www.gravatar.com/avatar/${md5(user.email)}?s=320`
    delete user.email
    dbres[i].author = user
  }
  let nextres = await database('r', 'SELECT * FROM posts ORDER BY pubdate DESC LIMIT 1 OFFSET ? ', [page * 10])
  return { list: dbres, next: !!nextres.length }
}