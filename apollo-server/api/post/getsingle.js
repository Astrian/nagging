const debug = require('debug')('fgm:api/user/get')
const database = require('../../plugins/database')
const md5 = require('md5')

module.exports = async (id) => {
  if (!id) throw new Error('请提供需要获取的碎碎念 ID。')
  let post = await database('r', 'SELECT * FROM posts WHERE id = ?', [id])
  if (!post[0]) throw new Error('找不到对应的碎碎念。')
  post = post[0]

  let user = (await database('r', 'SELECT username, fullname, avatar, email FROM users WHERE id = ?', [post.author]))[0]
  if (user.avatar === '!gravatar') user.avatar = `https://www.gravatar.com/avatar/${md5(user.email)}?s=320`
  delete user.email
  post.author = user

  return post
}