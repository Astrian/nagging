const debug = require('debug')('fgm:api/post/new')
const database = require('../../plugins/database')
const uuid = require('uuid')

module.exports = async (content, user) => {
  debug('Checking input...')
  if (!content) throw new Error('内容不能为空。')
  if (!user) throw new Error('找不到已登录用户。')

  debug('Write into database...')
  await database('w', 'INSERT INTO posts (id, author, content, pubdate) VALUES (?, ?, ?, ?)', [await postuuid(), user, content, parseInt(new Date().getTime())])

  debug('Modifing posts counter...')
  let dbres = await database('r', 'SELECT posts FROM users WHERE id = ?', [user])
  await database('w', 'UPDATE users SET posts = ? WHERE id = ?', [++dbres[0].posts, user])

  return
}

async function postuuid() {
  let newuuid = uuid.v4()
  let dbres = await database('r', 'SELECT id FROM posts WHERE id = ?', [newuuid])
  if (dbres.length) newuuid = postuuid()
  return newuuid
}