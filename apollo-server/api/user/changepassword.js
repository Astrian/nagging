const debug = require('debug')('fgm:api/user/changepassword')
const database = require('../../plugins/database')
const bcrypt = require('bcryptjs')

module.exports = async (user, body) => {
  debug('Checking input...')
  if (!body.newpassword) throw new Error('新密码不能为空。')
  if (!body.oldpassword) throw new Error('旧密码不能为空。')
  if (!user) throw new Error('找不到已登录用户。')

  debug('Getting information from database...')
  let dbres = await database('r', 'SELECT id, password FROM users WHERE id = ?', [user])
  if (!dbres[0]) throw new Error('找不到对应的用户。')

  debug('Comparing password...')
  if (!bcrypt.compareSync(body.oldpassword, dbres[0].password)) throw new Error('旧密码不正确。')

  debug('Encrypting new password...')
  let salt = bcrypt.genSaltSync(10)
  let password = bcrypt.hashSync(body.newpassword, salt)

  debug('Write new password...')
  await database('w', 'UPDATE users SET password = ? WHERE id = ?', [password, user])
  return
}