const debug = require('debug')('fgm:api/user/checksession')
const database = require('../../plugins/database')
const bcrypt = require('bcryptjs')

module.exports = async (auth) => {
  debug(auth)
  if (!auth) throw new Error('你还没有登录。')
  debug('Checking auth type...')
  auth = auth.split(' ')
  if (!auth[0] || !auth[1]) throw new Error('不是有效的登录凭证')
  if (auth[0] !== 'Basic') throw new Error('不是有效的登录凭证类型')

  debug('Decoding base64')
  debug(auth)
  auth = Buffer.from(auth[1], 'base64').toString('ascii')
  auth = auth.split(':')
  if (!auth[0] || !auth[1]) throw new Error('不是有效的登录凭证')
  debug(auth)

  debug('Getting data from database...')
  let dbres = await database('r', 'SELECT user, vaild, token FROM sessions WHERE id = ?', [auth[0]])
  if (!dbres.length) throw new Error('你还没有登录。')

  debug('Compairing token...')
  if (!bcrypt.compareSync(auth[1], dbres[0].token)) throw new Error('登录凭证不正确。')

  debug('Checking vaild date...')
  let vaild = false
  if (parseInt(dbres[0].vaild) === 0) vaild = true
  if (dbres[0].vaild > new Date().getTime()) vaild = true
  if (!vaild) throw new Error('登录凭证已过期。')

  return dbres[0].user
}