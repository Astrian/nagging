const debug = require('debug')('fgm:api/user/deletesession')
const database = require('../../plugins/database')

module.exports = async (id, user) => {
  debug(id,user)
  let dbres = await database('r', 'SELECT * FROM sessions WHERE id = ? AND user = ?', [id, user])
  if (!dbres[0]) throw new Error('未能找到对应的 session。')
  await database('w', 'DELETE FROM sessions WHERE id = ?', [id])
  return
}