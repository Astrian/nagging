const debug = require('debug')('fgm:api/user/generatesessions')
const database = require('../../plugins/database')
const bcrypt = require('bcryptjs')
const uuid = require('uuid')
const random = require('crypto-random-string')

module.exports = async (body, user) => {
  body.vaild = parseInt(body.vaild)
  debug(body)
  debug('Checking input...')
  let vaild = false
  if (parseInt(body.vaild) === 0) vaild = true
  if (parseInt(body.vaild) > new Date().getTime()) vaild = true
  if (!vaild) throw new Error('无效的令牌有效期。')
  if (!body.desc) throw new Error('请为令牌命名。')

  debug('Getting information from database...')
  let dbres = await database('r', 'SELECT id, password FROM users WHERE id = ?', [user])
  if (!dbres[0]) throw new Error('不存在此用户。')

  debug('Generating access token...')
  let token = random({length: 32, characters: 'abcdefghijklmnopqrstuvwxyz1234567890'})
  let salt = bcrypt.genSaltSync(10)
  let hashedtoken = bcrypt.hashSync(token, salt)

  debug('Generating session...')
  let session = await sessionuuid()
  
  debug('Writing access token to database...')
  await database('w', 'INSERT INTO sessions (id, user, desc, vaild, token) VALUES (?, ?, ?, ?, ?)', [session, user, body.desc, body.vaild, hashedtoken])

  return { session, token }
}

async function sessionuuid() {
  let newuuid = uuid.v4()
  let dbres = await database('r', 'SELECT id FROM sessions WHERE id = ?', [newuuid])
  if (dbres.length) newuuid = sessionuuid()
  return newuuid
}