const debug = require('debug')('fgm:api/user/get')
const database = require('../../plugins/database')
const md5 = require('md5')

module.exports = async () => {
  let user = await database('r', 'SELECT id, username, fullname, avatar, email, posts, bio FROM users')
  if (!user[0]) throw new Error('该站无用户。')
  user = user[0]

  if (user.avatar === '!gravatar') user.avatar = `https://www.gravatar.com/avatar/${md5(user.email)}?s=320`
  delete user.email

  return user
}