module.exports = {
  signup: require('./signup'),
  login: require('./login'),
  checksession: require('./checksession'),
  get: require('./get'),
  changepassword: require('./changepassword'),
  getsessions: require('./getsessions'),
  generatesessions: require('./generatesessions'),
  deletesession: require('./deletesession')
}