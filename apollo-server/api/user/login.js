const debug = require('debug')('fgm:api/user/login')
const database = require('../../plugins/database')
const bcrypt = require('bcryptjs')
const uuid = require('uuid')
const random = require('crypto-random-string')

module.exports = async (body, useragent) => {
  debug('Checking user information...')
  if (!body.username) throw new Error('请输入用户名。')
  if (!body.password) throw new Error('请输入密码。')

  debug('Getting information from database...')
  let dbres = await database('r', 'SELECT id, password FROM users WHERE username = ?', [body.username])
  if (!dbres[0]) throw new Error('不存在此用户名。')

  debug('Compairing password...')
  if (!bcrypt.compareSync(body.password, dbres[0].password)) throw new Error('密码不正确。')

  debug('Getting user agent...')
  useragent = `${getOperationSys(useragent)} 上的 ${getBrowser(useragent)}`

  debug('Generating access token...')
  let token = random({length: 32, characters: 'abcdefghijklmnopqrstuvwxyz1234567890'})
  let salt = bcrypt.genSaltSync(10)
  let hashedtoken = bcrypt.hashSync(token, salt)

  debug('Generating session...')
  let session = await sessionuuid()
  
  debug('Writing access token to database...')
  await database('w', 'INSERT INTO sessions (id, user, desc, vaild, token) VALUES (?, ?, ?, ?, ?)', [session, dbres[0].id, useragent, 0, hashedtoken])

  return { session, token }
}

async function sessionuuid() {
  let newuuid = uuid.v4()
  let dbres = await database('r', 'SELECT id FROM sessions WHERE id = ?', [newuuid])
  if (dbres.length) newuuid = sessionuuid()
  return newuuid
}

function getBrowser(UserAgent) {
  UserAgent = UserAgent.toLowerCase()
  if (UserAgent.indexOf('chrome') > -1 && UserAgent.indexOf('safari') > -1) return 'Google Chrome'
  else if (UserAgent.indexOf('firefox') > -1) return 'Firefox'
  else if (UserAgent.indexOf('opera') > -1) return 'Opera'
  else if (UserAgent.indexOf('safari') > -1 && UserAgent.indexOf('chrome') === -1) return 'Safari'
  else if (UserAgent.indexOf('edge') > -1) return 'Microsoft Edge'
  else if (/qqbrowser/.test(UserAgent)) return 'QQ 浏览器'
  else if (/MicroMessenger/i.test(UserAgent)) return '微信内建浏览器'
  else return '未知浏览器'
}

function getOperationSys(UserAgent) {
  if (UserAgent.indexOf('Win32') > -1 || UserAgent.indexOf('Windows') > -1) return 'Windows'
  else if (UserAgent.indexOf('OS X') > -1 || UserAgent.indexOf('Mac68K') > -1 || UserAgent.indexOf('MacPPC') > -1 || UserAgent.indexOf('Macintosh') > -1 || UserAgent.indexOf('MacIntel') > -1) return 'macOS'
  else if (UserAgent.indexOf('iPhone')) return 'iPhone'
  else if (UserAgent.indexOf('iPad')) return 'iPad'
  else if (UserAgent.indexOf('iPod')) return 'iPod'
  else if (UserAgent.indexOf('Android')) return 'Android'
  else return '未知设备'
}