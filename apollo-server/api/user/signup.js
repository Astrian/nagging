const debug = require('debug')('fgm:api/user/signup')
const database = require('../../plugins/database')
const bcrypt = require('bcryptjs')
const uuid = require('uuid')

module.exports = async (body) => {
  let dbres = await database('r', 'SELECT * FROM users', [])
  if (dbres.length) { throw new Error('该站点已停止注册新用户。') }

  debug('Checking new user information...')
  debug(body)
  if (!body.username) throw new Error('请指定新帐户的用户名。')
  if (!body.password) throw new Error('请指定新帐户的密码。')
  if (!body.email) throw new Error('请指定新帐户的邮箱地址。')

  debug('Encrypting password...')
  let salt = bcrypt.genSaltSync(10)
  let password = bcrypt.hashSync(body.password, salt)

  debug('Writing new user into database...')
  await database('w',
    'INSERT INTO users (id, username, fullname, avatar, telegramid, posts, password, email, bio) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
    [uuid.v4(), body.username, body.username, '!gravatar', null, 0, password, body.email, (body.bio || '')])

  return
}