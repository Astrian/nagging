export default ({ req, connection }) => {
  let returnArgs = {}
  returnArgs.authorization = req.headers.authorization || null
  returnArgs['user-agent'] = req.headers['user-agent'] || null
  return returnArgs
}
