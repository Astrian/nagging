const debug = require('debug')('fgm:plugins/database.js')
import sqlite from 'sqlite-async'

module.exports = async (ops, handle, params) => {
  try {
    // Import file
    let db = await sqlite.open('database.db')
    let dbres
    if (ops === 'w') dbres = await db.run(handle, params)
    else if (ops === 'r') dbres = await db.all(handle, params)
    else throw 'operation type error'
    await db.close()
    return dbres
  } catch (e) {
    throw e
  }
}
