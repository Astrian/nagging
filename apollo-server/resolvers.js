import GraphQLJSON from 'graphql-type-json'
import api from './api'


export default {
  JSON: GraphQLJSON,
  Query: {
    getUser: async () => await api.user.get(),
    getPosts: async (root, args) => await api.post.get(args.pager),
    getPost: async (root, args) => await api.post.getsingle(args.id),
    getSessions: async (root, args, ctx) => {
      let user = await api.user.checksession(ctx.authorization)
      return await api.user.getsessions(user)
    }
  },

  Mutation: {
    login: async (root, args, ctx) => await api.user.login(args, ctx['user-agent']),
    sendPost: async (root, {content}, ctx, fourth) => {
      let user = await api.user.checksession(ctx.authorization)
      await api.post.new(content, user)
      return null
    },
    signup: async (root, args, ctx, info) => await api.user.signup(args),
    changePassword: async (root, args, ctx) => {
      let user = await api.user.checksession(ctx.authorization)
      await api.user.changepassword(user, args)
      return null
    },
    generateSession: async (root, args, ctx) => {
      let user = await api.user.checksession(ctx.authorization)
      return await api.user.generatesessions(args, user)
    },
    revokeSession: async (root, {id}, ctx) => {
      let user = await api.user.checksession(ctx.authorization)
      await api.user.deletesession(id, user)
      return null
    }
  }
}
