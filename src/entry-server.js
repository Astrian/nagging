import 'isomorphic-fetch'
import ApolloSSR from 'vue-apollo/ssr'
import { createApp } from './main'

const prepareUrlForRouting = url => {
  const { BASE_URL } = process.env
  return url.startsWith(BASE_URL.replace(/\/$/, ''))
    ? url.substr(BASE_URL.length)
    : url
}

export default async context => {
  const { app, router, apolloProvider } = await createApp()
  const meta = app.$meta()
  return new Promise((resolve, reject) => {
    router.push(prepareUrlForRouting(context.url))
    context.meta = meta
    router.onReady(() => {
      context.rendered = () => {

        // Same for Apollo client cache
        context.apolloState = ApolloSSR.getStates(apolloProvider)
      }
      resolve(app)
    }, reject)
  })
}
