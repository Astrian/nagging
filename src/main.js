import '@babel/polyfill'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import { createRouter } from './router'
import { createProvider } from './vue-apollo'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'
import VueMeta from 'vue-meta'
import config from '@/config.js'

Vue.config.productionTip = false
Vue.prototype.$globalconfig = config
Vue.use(VueToast)
Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
})

export async function createApp ({
          beforeApp = () => {},
          afterApp = () => {}
        } = {}) {
          const router = createRouter()
          
          const apolloProvider = createProvider({
            ssr: process.server,
          })

          await beforeApp({
            router,
            
            apolloProvider,
          })

          const app = new Vue({
  router,
  apolloProvider,
  render: h => h(App)
})

          const result = {
            app,
            router,
            
            apolloProvider,
          }

          await afterApp(result)

          return result
        }
