import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Post from '../views/Post.vue'
import Security from '../views/Preference/Security.vue'

Vue.use(VueRouter)

export function createRouter() {
    const routes = [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/posts/:id',
      name: 'Post',
      component: Post
    },
    {
      path: '/preference/security',
      name: 'Security',
      component: Security
    }
  ]

    const router = new VueRouter({
      routes,
      mode: 'history'
    })

    return router;
}